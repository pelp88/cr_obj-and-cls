package ru.codeinside.lessons.javacore.implementation;

public class Company implements Service{
    String itn;
    String name;
    String accountId;

    @Override
    public String makeService() {
        return "Company created";
    }

    @Override
    public String getName() {
        return this.name + ", " + this.itn;
    }

    @Override
    public String getShortName() {
        return this.name;
    }
}
